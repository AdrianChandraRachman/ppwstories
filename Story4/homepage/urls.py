from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.story3, name='story3'),
    path('Contacts/', views.contacts, name='contacts'),
    path('story1/', views.story1, name='story1'),
]
