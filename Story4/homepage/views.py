from django.shortcuts import render

# Create your views here.
def story1(request):
    return render(request, 'story1.html')


def story3(request):
    return render(request, 'Story3.html')


def contacts(request):
    return render(request, 'Contacts.html')
